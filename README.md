# Simple Web Bin to debug http requests

# Getting started

```
source source.me
go run main.go
```

# Build the docker container:

docker build -t kstaykov/webin:latest .

# Run the container - example

```
$ docker run --rm -ti -p 9090:9090 kstaykov/webin 
--------------------------------------------
Form: map[]
Path: /
Scheme: 
Headers: map[Connection:[keep-alive] Cache-Control:[max-age=0] User-Agent:[Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36] Upgrade-Insecure-Requests:[1] Accept:[text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8] Accept-Encoding:[gzip, deflate, br] Accept-Language:[en-US,en;q=0.9,bg;q=0.8]]
Long URL of form: []
--------------------------------------------
Form: map[]
Path: /favicon.ico
Scheme: 
Headers: map[Cache-Control:[no-cache] Referer:[http://localhost:9090/] Pragma:[no-cache] Connection:[keep-alive] User-Agent:[Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36] Accept:[image/webp,image/apng,image/*,*/*;q=0.8] Accept-Encoding:[gzip, deflate, br] Accept-Language:[en-US,en;q=0.9,bg;q=0.8]]
Long URL of form: []

^C
$
```

# Clean-up docker images

```
docker image prune
```