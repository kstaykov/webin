FROM golang:1.9.7 as builder
WORKDIR /app/
COPY main.go /app/
RUN GOOS=linux GOARCH=amd64 CGO_ENABLED=0 GOPATH=`pwd` go build -o webin

FROM alpine:latest  
RUN apk --no-cache add ca-certificates
WORKDIR /root/
COPY --from=builder /app/webin .
CMD ["./webin"]  