package main

import (
	"fmt"
	"log"
	"net/http"
	"strings"
)

func dumpHttp(resp http.ResponseWriter, req *http.Request) {
        req.ParseForm() // Parse
        fmt.Println("--------------------------------------------")
        fmt.Println("Form:", req.Form) // print form information in server side
        fmt.Println("Path:", req.URL.Path)
        fmt.Println("Scheme:", req.URL.Scheme)
        fmt.Println("Headers:", req.Header)
        body, err := ioutil.ReadAll(req.Body)
        if err != nil {
          fmt.Println (err)
        }
        textBody := string(body)
        fmt.Println(textBody)
        fmt.Println("Long URL of form:", req.Form["url_long"])
        for k, v := range req.Form {
                fmt.Println("Form key:", k)
                fmt.Println("Form val:", strings.Join(v, ""))
        }
        fmt.Fprintf(resp, "Check console output.\n") // send data to client side
}


func main() {
	http.HandleFunc("/", dumpHttp)           // set router
	err := http.ListenAndServe(":9090", nil) // set listen port
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
